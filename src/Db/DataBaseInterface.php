<?php


namespace Kl\Db;


use Kl\Db\Table\TableInterface;
use Kl\Services\ModelConverter\ModelConverterInterface;

interface DataBaseInterface
{
    const TABLE_USER = 'user';
    const TABLE_PAYMENT = 'payment';

    public function __construct(ModelConverterInterface $modelConverter);

    public function addTable(string $tableName, TableInterface $table): self;

    public function getTable(string $tableName): TableInterface;

    public function saveModel(string $tableName, $model): bool;
}
