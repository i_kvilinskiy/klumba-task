<?php


namespace Kl\Db;


use Kl\Db\Table\TableInterface;
use Kl\Services\ModelConverter\ModelConverterInterface;

interface DataBaseBuilderInterface
{
    public function addTable($tableName, array $data = []): self;

    public function getDataBase(): DataBaseInterface;
}
