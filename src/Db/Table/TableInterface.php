<?php


namespace Kl\Db\Table;


use Kl\Services\ModelConverter\ModelConverterInterface;

/**
 * Interface TableInterface
 * @package Kl\Db\Table
 */
interface TableInterface
{
    /**
     * @param array $model
     * @return bool
     */
    public function saveModel(array $model): bool;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return array|null
     */
    public function findById(int $id);
}
