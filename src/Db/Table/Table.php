<?php


namespace Kl\Db\Table;


use Kl\Services\ModelConverter\ModelConverterInterface;

/**
 * Class Table
 * @package Kl\Db\Table
 */
class Table implements TableInterface
{
    /**
     * @var ModelConverterInterface
     */
    private $modelConverter;

    /**
     * @var array
     */
    private $storage = [];

    /**
     * @param array $model
     * @return bool
     */
    public function saveModel(array $model): bool
    {
        if (
            isset($model['id'])
            && ($idx = $this->getIdxById($model['id'])) !== null
        ) {
            $this->storage[$idx] = $model;
        } else {
            $this->storage[] = $model;
        }

        return true;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->storage;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function findById(int $id)
    {
        if (($idx = $this->getIdxById($id)) !== null) {
            return $this->storage[$idx];
        }

        return null;
    }

    /**
     * @param $id
     * @return int|null
     */
    protected function getIdxById($id)
    {
        foreach ($this->storage as $key => $item) {
            if (isset($item['id']) && $item['id'] == $id)
                return $key;
        }

        return null;
    }
}
