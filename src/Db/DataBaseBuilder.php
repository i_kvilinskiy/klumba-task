<?php


namespace Kl\Db;


use Kl\Db\Table\Table;
use Kl\Services\ModelConverter\ModelConverter;
use Kl\Services\ModelConverter\Strategy\ArrayCamelCaseStrategy;

/**
 * Class DataBaseBuilder
 * @package Kl\Db
 */
class DataBaseBuilder implements DataBaseBuilderInterface
{
    /**
     * @var DataBaseInterface
     */
    private $dataBase;

    /**
     * DataBaseBuilder constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * @param $tableName
     * @param array $data
     * @return DataBaseBuilderInterface
     */
    public function addTable($tableName, array $data = []): DataBaseBuilderInterface
    {
        $table = new Table();

        foreach ($data as $row)
            $table->saveModel($row);

        $this->dataBase->addTable($tableName, $table);

        return $this;
    }

    /**
     * @return DataBaseInterface
     */
    public function getDataBase(): DataBaseInterface
    {
        $result = $this->dataBase;
        $this->reset();

        return $result;
    }

    /**
     * @return void
     */
    private function reset()
    {
        $modelConverter = new ModelConverter(
            new ArrayCamelCaseStrategy()
        );

        $this->dataBase = new DataBase($modelConverter);
    }
}
