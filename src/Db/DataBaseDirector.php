<?php


namespace Kl\Db;


class DataBaseDirector
{
    /**
     * @var DataBaseBuilderInterface
     */
    private $builder;

    public function setBuilder(DataBaseBuilderInterface $builder): self
    {
        $this->builder = $builder;

        return $this;
    }

    public function buildTestDB(): DataBaseInterface
    {
        $this->builder
            ->addTable(DataBaseInterface::TABLE_USER, [
                [
                    'id' => 1,
                    'email' => 'testuser1@test.com',
                    'balance' => 120.45
                ],
                [
                    'id' => 2,
                    'email' => 'testuser2@test.com',
                    'balance' => 9999.45
                ],
                [
                    'id' => 3,
                    'email' => 'testuser3@test.com',
                    'balance' => 0.45
                ]
            ])
            ->addTable(DataBaseInterface::TABLE_PAYMENT)
        ;

        return $this->builder->getDataBase();
    }
}
