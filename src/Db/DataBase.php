<?php


namespace Kl\Db;


use Kl\Db\Table\TableInterface;
use Kl\Services\ModelConverter\ModelConverterInterface;

/**
 * Class DataBase
 * @package Kl\Db
 */
class DataBase implements DataBaseInterface
{
    /**
     * @var ModelConverterInterface
     */
    private $modelConverter;

    /**
     * DataBase constructor.
     * @param ModelConverterInterface $modelConverter
     */
    public function __construct(ModelConverterInterface $modelConverter)
    {
        $this->modelConverter = $modelConverter;
    }

    /**
     * @var TableInterface[]
     */
    private $tables = [];

    /**
     * @param string $tableName
     * @param TableInterface $table
     * @return DataBaseInterface
     */
    public function addTable(string $tableName, TableInterface $table): DataBaseInterface
    {
        $this->tables[$tableName] = $table;

        return $this;
    }

    /**
     * @param string $tableName
     * @param $model
     * @return bool
     * @throws \Exception
     */
    public function saveModel(string $tableName, $model): bool
    {
        if (!isset($this->tables[$tableName]))
            throw new \Exception('Table ' . $tableName . 'does not exists');

        $data = $this->modelConverter->convert($model);

        return $this->tables[$tableName]->saveModel($data);
    }

    /**
     * @param string $tableName
     * @return TableInterface
     * @throws \Exception
     */
    public function getTable(string $tableName): TableInterface
    {
        if (!isset($this->tables[$tableName]))
            throw new \Exception('Table ' . $tableName . 'does not exists');

        return $this->tables[$tableName];
    }

}
