<?php


namespace Kl\Models;


/**
 * Interface PaymentInterface
 * @package Kl\Models
 */
interface PaymentInterface extends ModelInterface
{
    const TYPE_IN = 'in';
    const TYPE_OUT = 'out';

    /**
     * @param int $userId
     * @return PaymentInterface
     */
    public function setUserId(int $userId): self;

    /**
     * @return int|null
     */
    public function getUserId();

    /**
     * @return string|null
     */
    public function getType();

    /**
     * @param float $balanceBefore
     * @return PaymentInterface
     */
    public function setBalanceBefore(float $balanceBefore): self;

    /**
     * @return float|null
     */
    public function getBalanceBefore();

    /**
     * @param float $amount
     * @return PaymentInterface
     */
    public function setAmount(float $amount): self;

    /**
     * @return float|null
     */
    public function getAmount();
}
