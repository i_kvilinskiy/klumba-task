<?php


namespace Kl\Models;


/**
 * Interface UserInterface
 * @package Kl\Models
 */
interface UserInterface extends ModelInterface
{
    /**
     * @param string $email
     * @return UserInterface
     */
    public function setEmail(string $email): self;

    /**
     * @return string|null
     */
    public function getEmail();

    /**
     * @param float $balance
     * @return UserInterface
     */
    public function setBalance(float $balance): self;

    /**
     * @return float|null
     */
    public function getBalance();
}
