<?php


namespace Kl\Models;


/**
 * Interface ModelInterface
 * @package Kl\Models
 */
interface ModelInterface
{
    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return ModelInterface
     */
    public function setId(int $id): self;
}
