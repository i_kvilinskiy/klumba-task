<?php


namespace Kl\Models;


/**
 * Class AbstractModel
 * @package Kl\Models
 */
abstract class AbstractModel implements ModelInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ModelInterface
     */
    public function setId(int $id): ModelInterface
    {
        $this->id = $id;

        return $this;
    }
}
