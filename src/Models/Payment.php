<?php


namespace Kl\Models;


/**
 * Class Payment
 * @package Kl\Models
 */
class Payment extends AbstractModel implements PaymentInterface
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var float
     */
    private $balanceBefore;

    /**
     * @var float
     */
    private $amount;

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return PaymentInterface
     */
    public function setUserId(int $userId): PaymentInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return float|null
     */
    public function getBalanceBefore()
    {
        return $this->balanceBefore;
    }

    /**
     * @param float $balanceBefore
     * @return PaymentInterface
     */
    public function setBalanceBefore(float $balanceBefore): PaymentInterface
    {
        $this->balanceBefore = $balanceBefore;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return PaymentInterface
     */
    public function setAmount(float $amount): PaymentInterface
    {
        $this->amount = $amount;

        $this->type = $amount > 0
            ? PaymentInterface::TYPE_IN
            : PaymentInterface::TYPE_OUT
        ;

        return $this;
    }
}
