<?php


namespace Kl\Models;


/**
 * Class User
 * @package Kl\Models
 */
class User extends AbstractModel implements UserInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var float
     */
    private $balance;

    /**
     * @param string $email
     * @return UserInterface
     */
    public function setEmail(string $email): UserInterface
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param float $balance
     * @return UserInterface
     */
    public function setBalance(float $balance): UserInterface
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getBalance()
    {
        return $this->balance;
    }

}
