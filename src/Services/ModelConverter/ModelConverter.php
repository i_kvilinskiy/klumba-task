<?php


namespace Kl\Services\ModelConverter;


use Kl\Services\ModelConverter\Strategy\ModelConverterStrategyInterface;

/**
 * Class ModelConverter
 * @package Kl\Services\ModelConverter
 */
class ModelConverter implements ModelConverterInterface
{
    /**
     * @var ModelConverterStrategyInterface
     */
    private $strategy;

    /**
     * ModelConverterInterface constructor.
     * @param ModelConverterStrategyInterface $strategy
     */
    public function __construct(ModelConverterStrategyInterface $strategy)
    {
        $this->setStrategy($strategy);
    }

    /**
     * @param ModelConverterStrategyInterface $strategy
     * @return ModelConverterInterface
     */
    public function setStrategy(ModelConverterStrategyInterface $strategy): ModelConverterInterface
    {
        $this->strategy = $strategy;

        return $this;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function convert($model): array
    {
        return $this->strategy->convert($model);
    }
}
