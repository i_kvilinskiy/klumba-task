<?php


namespace Kl\Services\ModelConverter\Strategy;


/**
 * Class ArrayUnderscoreStrategy
 * @package Kl\Services\ModelConverter\Strategy
 */
class ArrayUnderscoreStrategy extends AbstractModelConverterStrategy
{
    /**
     * @param $data
     * @return array
     */
    public function convert($data): array
    {
        $result = [];

        $data = $this->getModelData($data);

        foreach ($data as $field => $value) {
            $newField = preg_replace('/[^a-z0-9]+/i', ' ', $field);
            $newField = trim($newField);
            $newField = str_replace(' ', '_', $newField);
            $newField = strtolower($newField);
            $result[$newField] = $value;
        }

        return $result;
    }
}
