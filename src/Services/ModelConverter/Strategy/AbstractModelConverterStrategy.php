<?php


namespace Kl\Services\ModelConverter\Strategy;


/**
 * Class AbstractModelConverterStrategy
 * @package Kl\Services\ModelConverter\Strategy
 */
abstract class AbstractModelConverterStrategy implements ModelConverterStrategyInterface
{
    /**
     * @param $model
     * @return array
     */
    protected function getModelData($model): array
    {
        $data = [];
        $methods = get_class_methods($model);

        foreach ($methods as $method) {
            $match = [];
            preg_match("/^get*[A-Z]/", $method, $match);

            if (isset($match[0])) {
                $key = strtolower(substr($match[0], 3))
                    . substr($method, 4);

                $data[$key] = $model->$method();
            }
        }

        return $data;
    }
}
