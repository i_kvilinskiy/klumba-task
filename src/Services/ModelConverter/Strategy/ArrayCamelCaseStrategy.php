<?php


namespace Kl\Services\ModelConverter\Strategy;


/**
 * Class ArrayCamelCaseStrategy
 * @package Kl\Services\ModelConverter\Strategy
 */
class ArrayCamelCaseStrategy extends AbstractModelConverterStrategy
{
    /**
     * @param $data
     * @return array
     */
    public function convert($data): array
    {
        $result = [];

        $data = $this->getModelData($data);

        foreach ($data as $field => $value) {
            $newField = preg_replace('/[^a-z0-9]+/i', ' ', $field);
            $newField = trim($newField);
            $newField = ucwords($newField);
            $newField = str_replace(' ', '', $newField);
            $newField = lcfirst($newField);

            $result[$newField] = $value;
        }

        return $result;
    }
}
