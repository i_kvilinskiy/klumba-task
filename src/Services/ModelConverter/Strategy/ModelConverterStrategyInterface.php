<?php


namespace Kl\Services\ModelConverter\Strategy;


/**
 * Interface ModelConverterStrategyInterface
 * @package Kl\Services\ModelConverter\Strategy
 */
interface ModelConverterStrategyInterface
{
    /**
     * @param $data
     * @return array
     */
    public function convert($data): array;
}
