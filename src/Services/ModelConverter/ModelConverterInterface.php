<?php


namespace Kl\Services\ModelConverter;


use Kl\Services\ModelConverter\Strategy\ModelConverterStrategyInterface;

/**
 * Interface ModelConverterInterface
 * @package Kl\Services\ModelConverter
 */
interface ModelConverterInterface
{
    /**
     * ModelConverterInterface constructor.
     * @param ModelConverterStrategyInterface $strategy
     */
    public function __construct(ModelConverterStrategyInterface $strategy);

    /**
     * @param ModelConverterStrategyInterface $strategy
     * @return ModelConverterInterface
     */
    public function setStrategy(ModelConverterStrategyInterface $strategy): self;

    /**
     * @param $model
     * @return array
     */
    public function convert($model): array;
}
