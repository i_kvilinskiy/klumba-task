<?php


namespace Kl\Services\UserPaymentService;


use Kl\Db\DataBaseInterface;
use Kl\Models\Payment;
use Kl\Models\UserInterface;
use Kl\Services\Mailer\MailBuilderInterface;

/**
 * Class UserPaymentService
 * @package Kl\Services\UserPaymentService
 */
class UserPaymentService implements UserPaymentServiceInterface
{
    /**
     * @var DataBaseInterface
     */
    private $dataBase;

    /**
     * @var MailBuilderInterface
     */
    private $mailer;

    /**
     * UserPaymentService constructor.
     * @param DataBaseInterface $dataBase
     * @param MailBuilderInterface $mailer
     */
    public function __construct(
        DataBaseInterface $dataBase,
        MailBuilderInterface $mailer
    ) {
        $this->dataBase = $dataBase;
        $this->mailer = $mailer;
    }

    /**
     * @param UserInterface $user
     * @param float $amount
     * @return bool
     */
    public function changeBalance(UserInterface $user, float $amount)
    {
        $payment = new Payment();
        $payment
            ->setAmount($amount)
            ->setUserId($user->getId())
            ->setBalanceBefore($user->getBalance())
        ;

        $user->setBalance(
            $user->getBalance() + $amount
        );

        $this->dataBase->saveModel(DataBaseInterface::TABLE_USER, $user);
        $this->dataBase->saveModel(DataBaseInterface::TABLE_PAYMENT, $payment);

        if (($userEmail = $user->getEmail()) !== null) {
            $this->mailer
                ->setFrom('admin@test.com')
                ->setTo($userEmail)
                ->setSubject('Balance update')
                ->setMessage('Hello! Your balance has been successfully updated!')
                ->getMail()
                ->send()
            ;
        }

        return true;
    }
}
