<?php


namespace Kl\Services\UserPaymentService;


use Kl\Db\DataBaseInterface;
use Kl\Models\UserInterface;
use Kl\Services\Mailer\MailBuilderInterface;

/**
 * Interface UserPaymentServiceInterface
 * @package Kl\Services\UserPaymentService
 */
interface UserPaymentServiceInterface
{
    /**
     * UserPaymentServiceInterface constructor.
     * @param DataBaseInterface $dataBase
     * @param MailBuilderInterface $mailer
     */
    public function __construct(
        DataBaseInterface $dataBase,
        MailBuilderInterface $mailer
    );

    /**
     * @param UserInterface $user
     * @param float $amount
     * @return bool
     */
    public function changeBalance(UserInterface $user, float $amount);
}
