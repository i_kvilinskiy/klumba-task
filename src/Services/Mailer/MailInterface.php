<?php


namespace Kl\Services\Mailer;


/**
 * Interface MailInterface
 * @package Kl\Services\Mailer
 */
interface MailInterface
{
    /**
     * @param string $from
     * @return MailInterface
     */
    public function setFrom(string $from): self;

    /**
     * @param string $to
     * @return MailInterface
     */
    public function setTo(string $to): self;

    /**
     * @param string $subject
     * @return MailInterface
     */
    public function setSubject(string $subject): self;

    /**
     * @param string $message
     * @return MailInterface
     */
    public function setMessage(string $message): self;

    /**
     * @param string $header
     * @return MailInterface
     */
    public function addHeader(string $header): self;

    /**
     * @return bool
     */
    public function send(): bool;
}
