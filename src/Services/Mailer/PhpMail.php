<?php


namespace Kl\Services\Mailer;


use Kl\Models\AbstractModel;

/**
 * Class PhpMail
 * @package Kl\Services\Mailer
 */
class PhpMail extends AbstractMail implements MailInterface
{
    /**
     * @return bool
     */
    public function send(): bool
    {
        return mail(
            $this->to,
            $this->subject,
            $this->message,
            implode("\r\n", $this->headers)
        );
    }
}
