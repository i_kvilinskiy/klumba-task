<?php


namespace Kl\Services\Mailer;


/**
 * Class AbstractMail
 * @package Kl\Services\Mailer
 */
abstract class AbstractMail implements MailInterface
{
    /**
     * @var
     */
    protected $from;
    /**
     * @var
     */
    protected $to;
    /**
     * @var
     */
    protected $subject;
    /**
     * @var
     */
    protected $message;
    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @param string $from
     * @return MailInterface
     */
    public function setFrom(string $from): MailInterface
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param string $to
     * @return MailInterface
     */
    public function setTo(string $to): MailInterface
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param string $subject
     * @return MailInterface
     */
    public function setSubject(string $subject): MailInterface
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $message
     * @return MailInterface
     */
    public function setMessage(string $message): MailInterface
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param string $header
     * @return MailInterface
     */
    public function addHeader(string $header): MailInterface
    {
        $this->headers[] = $header;

        return $this;
    }
}
