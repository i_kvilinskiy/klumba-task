<?php


namespace Kl\Services\Mailer;


/**
 * Class PhpMailBuilder
 * @package Kl\Services\Mailer
 */
class PhpMailBuilder implements MailBuilderInterface
{
    /**
     * @var PhpMail
     */
    private $mail;

    /**
     * PhpMailBuilder constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * @param string $from
     * @return MailBuilderInterface
     */
    public function setFrom(string $from): MailBuilderInterface
    {
        $this->mail->setFrom($from);

        $this->mail
            ->addHeader('From: ' . $from)
            ->addHeader('Reply-To: ' . $from)
        ;

        return $this;
    }

    /**
     * @param string $to
     * @return MailBuilderInterface
     */
    public function setTo(string $to): MailBuilderInterface
    {
        $this->mail->setTo($to);

        return $this;
    }

    /**
     * @param string $subject
     * @return MailBuilderInterface
     */
    public function setSubject(string $subject): MailBuilderInterface
    {
        $this->mail->setSubject($subject);

        return $this;
    }

    /**
     * @param string $message
     * @return MailBuilderInterface
     */
    public function setMessage(string $message): MailBuilderInterface
    {
        $this->mail->setMessage($message);

        return $this;
    }

    /**
     * @param string $header
     * @return MailBuilderInterface
     */
    public function addHeader(string $header): MailBuilderInterface
    {
        $this->mail->addHeader($header);

        return $this;
    }

    /**
     * @return MailInterface
     */
    public function getMail(): MailInterface
    {
        $mail = $this->mail;
        $this->reset();

        return $mail;
    }

    /**
     * @return void
     */
    private function reset() {
        $this->mail = new PhpMail();
        $this->mail->addHeader('X-Mailer: PHP/' . phpversion());
    }
}
