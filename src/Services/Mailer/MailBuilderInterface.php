<?php


namespace Kl\Services\Mailer;


/**
 * Interface MailBuilderInterface
 * @package Kl\Services\Mailer
 */
interface MailBuilderInterface
{
    /**
     * @param string $from
     * @return MailBuilderInterface
     */
    public function setFrom(string $from): self;

    /**
     * @param string $to
     * @return MailBuilderInterface
     */
    public function setTo(string $to): self;

    /**
     * @param string $subject
     * @return MailBuilderInterface
     */
    public function setSubject(string $subject): self;

    /**
     * @param string $message
     * @return MailBuilderInterface
     */
    public function setMessage(string $message): self;

    /**
     * @param string $header
     * @return MailBuilderInterface
     */
    public function addHeader(string $header): self;

    /**
     * @return MailInterface
     */
    public function getMail(): MailInterface;
}
