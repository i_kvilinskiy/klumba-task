<?php


namespace tests;


use Kl\Db\DataBaseBuilder;
use Kl\Db\DataBaseDirector;
use Kl\Db\DataBaseInterface;
use Kl\Models\User;
use Kl\Services\Mailer\PhpMailBuilder;
use Kl\Services\UserPaymentService\UserPaymentService;
use Kl\Services\UserPaymentService\UserPaymentServiceInterface;
use PHPUnit\Framework\TestCase;

class UserPaymentServiceTests extends TestCase
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var UserPaymentServiceInterface
     */
    private $userPaymentService;

    /**
     * @var DataBaseInterface
     */
    private $dataBase;

    /**
     * @void
     */
    public function setUp()
    {
        $this->data = require_once "fixtures/data.php";

        $dbDirector = new DataBaseDirector();
        $dbDirector->setBuilder(new DataBaseBuilder());
        $this->dataBase = $dbDirector->buildTestDB();

        $this->userPaymentService = new UserPaymentService(
            $this->dataBase,
            new PhpMailBuilder()
        );
    }

    public function testChangeBalance()
    {
        foreach ($this->data as $item) {
            list($userData, $amount) = $item;

            $expectedBalance = $userData['balance'] + $amount;

            $user = new User();
            $user
                ->setBalance($userData['balance'])
                ->setEmail($userData['email'])
                ->setId($userData['id'])
            ;

            $this->userPaymentService
                ->changeBalance(
                    $user,
                    $amount
                )
            ;

            $dbRow = $this->dataBase
                ->getTable(DataBaseInterface::TABLE_USER)
                ->findById($user->getId())
            ;

            $this->assertEquals($expectedBalance, $user->getBalance());
            $this->assertEquals($expectedBalance, $dbRow['balance']);
        }
    }
}
